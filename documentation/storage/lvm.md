<!-- markdownlint-disable-next-line MD041 -->
## storage/lvm/remote

LVM test plan

- enabled: `true`
- provision:
  - how: connect
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests>
  - test:
    - /storage/lvm/cache
    - /storage/lvm/thinp/stqe
    - /storage/lvm/device-mapper-persistent-data
    - /storage/lvm/snapper
