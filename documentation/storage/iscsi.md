<!-- markdownlint-disable-next-line MD041 -->
## storage/iscsi/local

iSCSI tests running on single host

- enabled: `true`
- provision:
  - how: virtual
- discover:
  - filter: `['tag:iscsi', 'tag:local']`
