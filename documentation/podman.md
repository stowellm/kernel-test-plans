<!-- markdownlint-disable-next-line MD041 -->
## podman/podman

Podman userspace package test

- enabled: `true`
- provision:
  - how: virtual
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /container/podman/root$
    - /container/podman/rootless$
